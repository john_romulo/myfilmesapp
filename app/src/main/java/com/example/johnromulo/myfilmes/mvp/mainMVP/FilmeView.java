package com.example.johnromulo.myfilmes.mvp.mainMVP;

import android.content.Intent;
import android.net.ConnectivityManager;
import com.example.johnromulo.myfilmes.domains.Filme;

/**
 * Created by trinitymacmini on 30/10/17.
 */

public interface FilmeView {
    String FILMES_KEY = "filmes";
    void updateListaRecycler();
    void showToast( String mensagem );
    void openDetalhe(Intent intent);
    void openDetalhe(Filme filme);
    ConnectivityManager checkNotworkAccess();
    void showProgressBar( int visibilidade );
}
