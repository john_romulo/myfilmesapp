package com.example.johnromulo.myfilmes.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import com.example.johnromulo.myfilmes.R;
import com.example.johnromulo.myfilmes.adapter.FilmeAdapter;
import com.example.johnromulo.myfilmes.domains.Filme;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmePresenter;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmeView;
import com.example.johnromulo.myfilmes.mvp.presenters.FilmePresenterImpl;

public class MainActivity extends AppCompatActivity implements FilmeView {

    private static FilmePresenter filmePresenter;
    private FilmeAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if( filmePresenter == null ){
            filmePresenter = new FilmePresenterImpl();
        }

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems(savedInstanceState);
            }
        });

        filmePresenter.setView( this );
        filmePresenter.bucarListaFilmesServer( savedInstanceState );
    }



    void refreshItems(Bundle savedInstanceState) {
        filmePresenter.bucarListaFilmesServer( savedInstanceState );
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        RecyclerView rvFilmes = (RecyclerView) findViewById(R.id.rv_filmes);
        rvFilmes.setHasFixedSize(true);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL);
        rvFilmes.setLayoutManager( layoutManager );
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        adapter = new FilmeAdapter( this, filmePresenter.getFilmes() );
        rvFilmes.setAdapter( adapter );

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(FILMES_KEY, filmePresenter.getFilmes());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void updateListaRecycler(){
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showToast( String mensagem ){
        Toast.makeText(this, mensagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openDetalhe(Filme filme){
        filmePresenter.openDetalhe(filme);
    }

    @Override
    public void openDetalhe(Intent intent){
        startActivity(intent);
    }

    @Override
    public ConnectivityManager checkNotworkAccess() {
        return (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void showProgressBar( int visibilidade ){
        findViewById(R.id.pb_loading).setVisibility( visibilidade );
    }

}
