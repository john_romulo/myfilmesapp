package com.example.johnromulo.myfilmes.domains;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by johnromulo on 28/10/17.
 */

public class Filme implements Parcelable {

    private int id;
    private String nome;
    private int ano;
    private String urlTrailer;
    private String urlImg;
    private String sinopse;
    private String tipo;
    private int temporada;
    private String genero;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getUrlTrailer() {
        return urlTrailer;
    }

    public void setUrlTrailer(String urlTrailer) {
        this.urlTrailer = urlTrailer;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getTemporada() {
        return temporada;
    }

    public void setTemporada(int temporada) {
        this.temporada = temporada;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }


    public Filme(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Filme(Parcel source) {
        this.id = source.readInt();
        this.nome = source.readString();
        this.ano = source.readInt();
        this.urlTrailer = source.readString();
        this.urlImg = source.readString();
        this.sinopse = source.readString();
        this.tipo = source.readString();
        this.temporada = source.readInt();
        this.genero = source.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.nome);
        parcel.writeInt(this.ano);
        parcel.writeString(this.urlTrailer);
        parcel.writeString(this.urlImg);
        parcel.writeString(this.sinopse);
        parcel.writeString(this.tipo);
        parcel.writeInt(this.temporada);
        parcel.writeString(this.genero);
    }

    public static final Parcelable.Creator<Filme> CREATOR = new Parcelable.Creator<Filme>() {
        @Override
        public Filme createFromParcel(Parcel source) {
            return new Filme(source);
        }

        @Override
        public Filme[] newArray(int size) {
            return new Filme[size];
        }
    };
}
