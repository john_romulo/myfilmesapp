package com.example.johnromulo.myfilmes.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.johnromulo.myfilmes.R;
import com.example.johnromulo.myfilmes.activities.MainActivity;
import com.example.johnromulo.myfilmes.domains.Filme;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by johnromulo on 28/10/17.
 */

public class FilmeAdapter extends RecyclerView.Adapter<FilmeAdapter.ViewHolder> {

    private MainActivity activity;
    private ArrayList<Filme> filmes;

    public FilmeAdapter( MainActivity activity, ArrayList<Filme> filmes ){
        this.activity = activity;
        this.filmes = filmes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from( parent.getContext() )
                .inflate(R.layout.item_filme, parent, false);
        ViewHolder viewHolder = new ViewHolder( view );
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setDados( filmes.get( position ) );
    }

    @Override
    public int getItemCount() {
        return filmes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imgFilme;
        private TextView txTitulo;
        private TextView txAno;

        private ViewHolder(View itemView) {
            super(itemView);
            imgFilme =  itemView.findViewById(R.id.img_filme);
            txTitulo = itemView.findViewById(R.id.tx_titulo);
            txAno = itemView.findViewById(R.id.tx_ano);
        }

        private void setDados( Filme filme ){
            Picasso.with( imgFilme.getContext() )
                    .load( filme.getUrlImg() )
                    .into( imgFilme );
            txTitulo.setText( ""+filme.getNome() );
            txAno.setText( ""+filme.getAno() );
            imgFilme.setOnClickListener( this );

        }

        @Override
        public void onClick(View view) {
            activity.openDetalhe(filmes.get( getAdapterPosition()));
        }
    }
}
