package com.example.johnromulo.myfilmes.mvp.mainMVP;

import android.content.Context;
import android.os.Bundle;
import com.example.johnromulo.myfilmes.domains.Filme;
import java.util.ArrayList;

/**
 * Created by trinitymacmini on 30/10/17.
 */

public interface FilmePresenter {
    void bucarListaFilmesServer(Bundle savedInstanceState);
    Context getContext();
    void setView( FilmeView view );
    void updateListaRecycler( ArrayList<Filme> filmeArrayList );
    ArrayList<Filme> getFilmes();
    void showToast(String mensagem);
    void openDetalhe (Filme filme);
    void showProgressBar( boolean status );
}
