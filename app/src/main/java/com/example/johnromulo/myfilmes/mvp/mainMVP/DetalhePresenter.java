package com.example.johnromulo.myfilmes.mvp.mainMVP;

import android.content.Intent;
import com.example.johnromulo.myfilmes.domains.Filme;

/**
 * Created by trinitymacmini on 30/10/17.
 */

public interface DetalhePresenter {
    boolean checkNotworkAccess();
    Filme getFilme(Intent intent);
    void setView(DetalheView view);
}
