package com.example.johnromulo.myfilmes.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.example.johnromulo.myfilmes.R;
import com.example.johnromulo.myfilmes.domains.Filme;
import com.example.johnromulo.myfilmes.mvp.mainMVP.DetalhePresenter;
import com.example.johnromulo.myfilmes.mvp.mainMVP.DetalheView;
import com.example.johnromulo.myfilmes.mvp.presenters.DetalhePresenterImpl;

public class DetalheActivity extends AppCompatActivity implements DetalheView {

    private WebView webview;
    private Filme filme;
    private TextView txSinopse;

    private static DetalhePresenter detalhePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if(detalhePresenter == null){
            detalhePresenter = new DetalhePresenterImpl();
        }

        detalhePresenter.setView( this );
        filme =  detalhePresenter.getFilme(getIntent());

        if(filme != null){
            txSinopse = (TextView) findViewById(R.id.tx_sinopse);
            txSinopse.setText(""+filme.getSinopse());
        }

        webview = (WebView) findViewById(R.id.webview);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if(detalhePresenter.checkNotworkAccess()){
            webview.setWebViewClient(new WebViewClient());
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webview.setWebChromeClient(new WebChromeClient());
            webview.loadUrl("https://www.youtube.com/embed/"+filme.getUrlTrailer()+"?autoplay=1&controls=0&loop=0&rel=0&showinfo=0&iv_load_policy=3");
        }else {

            findViewById(R.id.rl_video).setBackgroundColor(Color.parseColor("#000000"));
        }
    }

    @Override
    public ConnectivityManager checkNotworkAccess() {
        return (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }
}
