package com.example.johnromulo.myfilmes.mvp.presenters;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import com.example.johnromulo.myfilmes.R;
import com.example.johnromulo.myfilmes.activities.DetalheActivity;
import com.example.johnromulo.myfilmes.domains.Filme;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmeModel;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmePresenter;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmeView;
import com.example.johnromulo.myfilmes.mvp.models.FilmeModelImpl;
import java.util.ArrayList;

/**
 * Created by johnromulo on 28/10/17.
 */

public class FilmePresenterImpl implements FilmePresenter {

    private FilmeModel filmesModel;
    private FilmeView filmesViews;
    private ArrayList<Filme> filmeArrayList = new ArrayList<>();

    public FilmePresenterImpl(){
        filmesModel = new FilmeModelImpl( this );
    }

    @Override
    public void bucarListaFilmesServer(Bundle savedInstanceState) {
        if( savedInstanceState != null ){
            filmeArrayList = savedInstanceState.getParcelableArrayList( FilmeView.FILMES_KEY );
        }
        if(checkNotworkAccess()){
            filmesModel.bucarListaFilmesServer();
        }else {
            showToast(getContext().getString(R.string.no_connection));
        }
    }

    @Override
    public Context getContext() {
        return (Context) filmesViews;
    }

    @Override
    public void setView(FilmeView view) {
        if(view != null){
            this.filmesViews = view;
        }
    }

    @Override
    public void updateListaRecycler(ArrayList<Filme> filmeArrayList) {
        this.filmeArrayList.clear();
        this.filmeArrayList.addAll( filmeArrayList );
        filmesViews.updateListaRecycler();
    }

    @Override
    public ArrayList<Filme> getFilmes() {
        return this.filmeArrayList;
    }

    @Override
    public void showToast(String mensagem) {
        filmesViews.showToast( mensagem );
    }

    @Override
    public void openDetalhe(Filme filme) {
        Intent intent = new Intent(getContext(), DetalheActivity.class);
        intent.putExtra("filme",filme);
        this.filmesViews.openDetalhe(intent);
    }

    public boolean checkNotworkAccess(){
        ConnectivityManager cm = filmesViews.checkNotworkAccess();
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void showProgressBar(boolean status) {
        int visibilidade = status ? View.VISIBLE : View.GONE;
        filmesViews.showProgressBar( visibilidade );
    }

}
