package com.example.johnromulo.myfilmes.services;

import com.example.johnromulo.myfilmes.domains.Filme;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmePresenter;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;
import cz.msebera.android.httpclient.Header;

/**
 * Created by johnromulo on 28/10/17.
 */

public class JsonFilmesHttpService extends JsonHttpResponseHandler {

    public static final String URI = "https://habit-forming-lette.000webhostapp.com/testeapp.php";
    private FilmePresenter filmesPresenter;

    public JsonFilmesHttpService (FilmePresenter filmesPresenter){
        this.filmesPresenter = filmesPresenter;
    }

    @Override
    public void onStart() {
        filmesPresenter.showProgressBar( true );
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
        Gson gson = new Gson();
        ArrayList<Filme> filmes = new ArrayList<>();
        Filme filme;

        for( int i = 0; i < response.length(); i++ ){
            try{
                filme = gson.fromJson( response.getJSONObject( i ).toString(), Filme.class );
                filmes.add( filme );
            }
            catch(JSONException e){}
        }

        filmesPresenter.updateListaRecycler( filmes );
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        filmesPresenter.showToast( responseString );
    }

    @Override
    public void onFinish() {
        filmesPresenter.showProgressBar( false );
    }

}
