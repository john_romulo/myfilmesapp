package com.example.johnromulo.myfilmes.mvp.presenters;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.johnromulo.myfilmes.domains.Filme;
import com.example.johnromulo.myfilmes.mvp.mainMVP.DetalhePresenter;
import com.example.johnromulo.myfilmes.mvp.mainMVP.DetalheView;


/**
 * Created by johnromulo on 29/10/17.
 */

public class DetalhePresenterImpl implements DetalhePresenter {

    private Filme filme;
    private DetalheView detalheView;

    public DetalhePresenterImpl(){

    }

    @Override
    public Filme getFilme(Intent intent){
        filme = intent.getParcelableExtra("filme");
        return filme;
    }

    @Override
    public void setView(DetalheView view) {
        if(view != null){
            this.detalheView = view;
        }
    }

    public boolean checkNotworkAccess(){
        ConnectivityManager cm = detalheView.checkNotworkAccess();
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
