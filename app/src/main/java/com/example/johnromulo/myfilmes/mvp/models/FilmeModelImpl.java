package com.example.johnromulo.myfilmes.mvp.models;

import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmeModel;
import com.example.johnromulo.myfilmes.mvp.mainMVP.FilmePresenter;
import com.example.johnromulo.myfilmes.services.JsonFilmesHttpService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

/**
 * Created by johnromulo on 28/10/17.
 */

public class FilmeModelImpl implements FilmeModel {

    private AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
    private FilmePresenter filmesPresenter;

    public FilmeModelImpl(FilmePresenter filmesPresenter){
        this.filmesPresenter = filmesPresenter;
    }

    @Override
    public void bucarListaFilmesServer(){
        RequestParams requestParams = new RequestParams();
        asyncHttpClient.get( filmesPresenter.getContext(),
                JsonFilmesHttpService.URI,
                requestParams,
                new JsonFilmesHttpService( filmesPresenter ));
    }

}
